<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181123095707 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE account_transaction_account_transaction_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE account_transaction_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE account_transaction_account_transaction_type (id INT NOT NULL, account_transaction_id INT NOT NULL, account_transaction_type_id INT NOT NULL, is_active BOOLEAN DEFAULT \'true\' NOT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DCC553AB7D370F05 ON account_transaction_account_transaction_type (account_transaction_id)');
        $this->addSql('CREATE INDEX IDX_DCC553AB387F8B02 ON account_transaction_account_transaction_type (account_transaction_type_id)');
        $this->addSql('CREATE TABLE account_transaction_type (id INT NOT NULL, is_active BOOLEAN DEFAULT \'true\' NOT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE account_transaction_account_transaction_type ADD CONSTRAINT FK_DCC553AB7D370F05 FOREIGN KEY (account_transaction_id) REFERENCES account_transaction (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_transaction_account_transaction_type ADD CONSTRAINT FK_DCC553AB387F8B02 FOREIGN KEY (account_transaction_type_id) REFERENCES account_transaction_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE account_transaction_account_transaction_type DROP CONSTRAINT FK_DCC553AB387F8B02');
        $this->addSql('DROP SEQUENCE account_transaction_account_transaction_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE account_transaction_type_id_seq CASCADE');
        $this->addSql('DROP TABLE account_transaction_account_transaction_type');
        $this->addSql('DROP TABLE account_transaction_type');
    }
}

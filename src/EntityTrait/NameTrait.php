<?php

namespace App\EntityTrait;

trait NameTrait
{
    /**
     * @var string Name
     */
    protected $name;

    /**
     * Sets name
     *
     * @param string $name Name
     *
     * @return self This object
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string Name
     */
    public function getName(): string
    {
        return $this->name;
    }
}
<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;

/**
 * Account transaction
 */
class AccountTransaction
{
    use BaseTrait;

    /**
     * @var int Account ID
     */
    private $accountId;
    
    /**
     * @var string Amount
     */
    private $amount;

    /**
     * @var int Account transaction type ID
     */
    private $accountTransactionTypeId;

    /**
     * @var Account Account
     */
    private $account;

    /**
     * @var AccountTransactionType Account transaction type
     */
    private $accountTransactionType;
    
    /**
     * Sets account ID
     *
     * @param int $accountId Account ID
     *
     * @return AccountTransaction This object
     */
    public function setAccountId(int $accountId): AccountTransaction
    {
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * Gets account ID
     *
     * @return int Account ID
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }
    
    /**
     * Sets amount
     *
     * @param string $amount Amount
     *
     * @return AccountTransaction This object
     */
    public function setAmount(string $amount): AccountTransaction
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Gets amount
     *
     * @return string Amount
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * Sets account transaction type ID
     *
     * @param int $accountTransactionTypeId Account transaction type ID
     *
     * @return AccountTransaction This object
     */
    public function setAccountTransactionTypeId(int $accountTransactionTypeId): AccountTransaction
    {
        $this->accountTransactionTypeId = $accountTransactionTypeId;

        return $this;
    }

    /**
     * Gets account transaction type ID
     *
     * @return int Account transaction type ID
     */
    public function getAccountTransactionTypeId(): int
    {
        return $this->accountTransactionTypeId;
    }

    /**
     * Sets account
     *
     * @param Account $account Account
     *
     * @return AccountTransaction This object
     */
    public function setAccount(Account $account): AccountTransaction
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Gets account
     *
     * @return Account Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * Sets accountTransactionType
     *
     * @param AccountTransactionType $accountTransactionType AccountTransactionType
     *
     * @return AccountTransaction This object
     */
    public function setAccountTransactionType(AccountTransactionType $accountTransactionType): AccountTransaction
    {
        $this->accountTransactionType = $accountTransactionType;

        return $this;
    }

    /**
     * Gets accountTransactionType
     *
     * @return AccountTransactionType AccountTransactionType
     */
    public function getAccountTransactionType(): AccountTransactionType
    {
        return $this->accountTransactionType;
    }
}
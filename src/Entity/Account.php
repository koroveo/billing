<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;

/**
 * Account
 */
class Account
{
    use BaseTrait;

    /**
     * @var string Balance
     */
    private $balance;

    /**
     * @var string Price
     */
    private $price;

    /**
     * Sets balance
     *
     * @param string $balance Balance
     *
     * @return Account This object
     */
    public function setBalance(string $balance): Account
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Gets balance
     *
     * @return string Balance
     */
    public function getBalance(): string
    {
        return $this->balance;
    }

    /**
     * Sets price
     *
     * @param string $price Price
     *
     * @return Account This object
     */
    public function setPrice(string $price): Account
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Gets price
     *
     * @return string Price
     */
    public function getPrice(): string
    {
        return $this->price;
    }
}
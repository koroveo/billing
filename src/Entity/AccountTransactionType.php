<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;
use App\EntityTrait\NameTrait;

/**
 * Account transaction type
 */
class AccountTransactionType
{
    use BaseTrait, NameTrait;
}
<?php

namespace App\Controller;

use App\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RobokassaController extends AbstractController
{
    public function getPaymentUrl()
    {
        $merchantLogin = 'reffection';
        $merchantPassword = 'LnaFjErpR7N0Cb6p36aP';
        $paymentId = 1;
        $paymentDescription = 'Пополнение счета';
        $isTest = 1;
        $defaultAmount = 1;
        $signature = md5("$merchantLogin::$paymentId:$merchantPassword");

        $paymentUrl = "https://auth.robokassa.ru/Merchant/PaymentForm/FormFLS.js?MerchantLogin=$merchantLogin&DefaultSum=$defaultAmount&InvoiceID=$paymentId&Description=$paymentDescription&SignatureValue=$signature&IsTest=$isTest";

        return new Response($paymentUrl);
    }

    public function result(Request $request)
    {
        $amount = $request->request->get('OutSum');

        $account = $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->find(1);

        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->deposit($account->getId(), 11, $amount);

        return new Response();
    }

    public function resultSuccess()
    {
        return $this->redirect('http://95.216.193.107/payment');
    }

    public function resultFail()
    {
        return new Response();
    }
}

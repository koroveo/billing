<?php

namespace App\Controller;

use App\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class WithdrawalController extends AbstractController
{
    public function byIdentifiedPhone(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->withdraw($accountId, 1, '2');

        return new Response();
    }

    public function byCollectedPhone(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->withdraw($accountId, 2, '2');

        return new Response();
    }

    public function byHandledIdentifiedPhone(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->withdraw($accountId, 3, '2');

        return new Response();
    }

    public function byHandledCollectedPhone(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->withdraw($accountId, 4, '2');

        return new Response();
    }

    public function byRedirectedCall(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->withdraw($accountId, 5, '2');

        return new Response();
    }
}

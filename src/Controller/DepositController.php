<?php

namespace App\Controller;

use App\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DepositController extends AbstractController
{
    public function byIdentifiedPhone(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->deposit($accountId, 6, '3');

        return new Response();
    }

    public function byCollectedPhone(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->deposit($accountId, 7, '3');

        return new Response();
    }

    public function byHandledIdentifiedPhone(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->deposit($accountId, 8, '3');

        return new Response();
    }

    public function byHandledCollectedPhone(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->deposit($accountId, 9, '3');

        return new Response();
    }

    public function byRedirectedCall(int $accountId): Response
    {
        $this
            ->getDoctrine()
            ->getRepository(Account::class)
            ->deposit($accountId, 10, '3');

        return new Response();
    }
}

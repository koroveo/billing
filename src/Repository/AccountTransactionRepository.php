<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\AccountTransaction;
use App\Entity\AccountTransactionType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AccountTransactionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccountTransaction::class);
    }

    public function createAccountTransaction(int $accountId, int $accountTransactionTypeId, string $amount): AccountTransaction
    {
        $account = $this
            ->getEntityManager()
            ->getRepository(Account::class)
            ->find($accountId);

        $accountTransactionType = $this
            ->getEntityManager()
            ->getRepository(AccountTransactionType::class)
            ->find($accountTransactionTypeId);

        $accountTransaction = new AccountTransaction();
        $accountTransaction->setAccount($account);
        $accountTransaction->setAccountTransactionType($accountTransactionType);
        $accountTransaction->setAmount($amount);

        $entityManager = $this->getEntityManager();
        $entityManager->persist($accountTransaction);
        $entityManager->flush();

        return $accountTransaction;
    }
}

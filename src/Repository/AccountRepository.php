<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\AccountTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AccountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Account::class);
    }

    public function deposit(int $accountId, int $accountTransactionTypeId, string $amount): Account
    {
        $entityManager = $this->getEntityManager();

        /** @var Account $account */
        $account = $this->find($accountId);

        $entityManager
            ->getRepository(AccountTransaction::class)
            ->createAccountTransaction($accountId, $accountTransactionTypeId, $amount);

        $balance = $account->getBalance() + $amount;
        $account->setBalance($balance);

        $entityManager = $this->getEntityManager();
        $entityManager->persist($account);
        $entityManager->flush();

        return $account;
    }

    public function withdraw(int $accountId, int $accountTransactionTypeId, string $amount): Account
    {
        $entityManager = $this->getEntityManager();

        /** @var Account $account */
        $account = $this->find($accountId);

        $entityManager
            ->getRepository(AccountTransaction::class)
            ->createAccountTransaction($accountId, $accountTransactionTypeId, $amount);

        $balance = $account->getBalance() - $amount;
        $account->setBalance($balance);

        $entityManager = $this->getEntityManager();
        $entityManager->persist($account);
        $entityManager->flush();

        return $account;
    }
}
